# --------------------------------------------------------------------------- #
# RUN THIS PROGRAM AT : http://www.codeskulptor.org/#user4-nr87HGAzT4fvD2X.py #
# --------------------------------------------------------------------------- #

import simplegui

# define global variables
interval = 100
tenth_of_second = 0
tries = 0 
score = 0
A, B, C, D = 0, 0, 0, 0
started = 0;

# Draw the canvas
def draw(canvas):
    canvas.draw_text(format(tenth_of_second), [65,100], 20, "White")
    canvas.draw_text(score_line(), [140,15], 15, "Green")

def score_line():
    return str(tries) + "/" + str(score)
    
# define helper function format that converts integer
# counting tenths of seconds into formatted string A:BC.D

def format(t):
    global A, B, C, D
    if (t > 1 and t < 10):
        D = t % 10
    elif (t >= 10 and t < 99):	
        C = int(t / 10) 
        D = t % 10
    elif (t >= 100 and t < 599):
        B = int(t / 100)
        t = t - (B*100)
        C = int(t / 10) 
        D = t % 10
    elif (t >= 600 and t < 59910):
        A = int(t / 600)
        t = t - (A*600)
        B = int(t / 100)
        t = t - (B*100)
        C = int(t / 10) 
        D = t % 10
    return str(A) + ":" + str(B) + str(C) + "." + str(D)

# define event handlers for buttons; "Start", "Stop", "Reset"
def start_button():
    global started
    started = 1
    timer.start()
    frame.set_draw_handler(draw)

def stop_button(): 
    global tries, score, started
    if (started != 0):     
        timer.stop()
        if (D==0 and (A>0 or B>0 or C>0)):
            score += 1
        tries += 1
    started = 0

def reset_button():
    global tries, score, started, tenth_of_second, A, B, C, D
    timer.stop()
    tries, score, started, tenth_of_second, A, B, C, D = 0, 0, 0, 0, 0, 0, 0, 0

    
# define event handler for timer with 0.1 sec interval
def tick(): 
    global tenth_of_second
    tenth_of_second += 1

# create frame
frame = simplegui.create_frame("Stopwatch: The Game", 200, 200)
timer = simplegui.create_timer(interval, tick )

start = frame.add_button("Start", start_button)
stop = frame.add_button("Stop", stop_button)
reset = frame.add_button("Reset", reset_button)

# start timer and frame
frame.start()